<?php

namespace PMC_SimpleGameMode;

use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\event\Listener;
use pocketmine\Player;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;

class PMC_SimpleGameMode extends PluginBase implements CommandExecutor, Listener {

    public function onEnable(){
        $this->getLogger()->info("§aSimpleGamemode включен!");
        $this->getServer()->getPluginManager()->registerEvents($this, $this);
    }

    public function onDisable(){
        $this->getLogger()->info("§cSimpleGamemode выключен!");
    }

    public function onCommand(CommandSender $sender, Command $command, $label, array $params){
        if(!($sender instanceof Player)){
            $sender->sendMessage(TextFormat::RED . "Используйте эту команду только в игре!");
            return true;
        }
        switch($command->getName()){
            case "gm":
                switch(array_shift($params)){
                    case "0":
                    case "s":
                    case "survival":
                        $sender->sendMessage(TextFormat::GREEN . "Вы изменили свой игровой режим на Выживание!");
                        $sender->setGamemode(0);
                        break;
                    case "1":
                    case "c":
                    case "creative":
                        $sender->sendMessage(TextFormat::GREEN . "Вы изменили свой игровой режим на Творческий!");
                        $sender->setGamemode(1);
                        break;
                    case "2":
                    case "a":
                    case "adventure":
                        $sender->sendMessage(TextFormat::GREEN . "Вы изменили свой игровой режим на Приключенческий!");
                        $sender->setGamemode(3);
                        break;
                    case "3":
                    case "sp":
                    case "spectator":
                        $sender->sendMessage(TextFormat::GREEN . "Вы изменили свой игровой режим на Наблюдение!");
                        $sender->setGamemode(3);
                        break;
                    default:
                        $sender->sendMessage(TextFormat::RED . "Неизвестный игровой режим!");
                        $sender->sendMessage(TextFormat::RED . "Доступные режимы: 0|s|survival 1|c|creative 2|a|adventure 2|sp|spectator");
                        break;
                }
                break;
        }
        return true;
    }
}
